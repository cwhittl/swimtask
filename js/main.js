var app = angular.module('swimTasksAng', []);
app.controller('DragDropCtrl', function($scope, service)
{
    $scope.items = service.getCardsForBoard();
    $scope.$watch('[items]', function()
    {
        Widget.init();
    });
});


app.filter('orderObjectBy', function()
{
    return function(items, field, reverse)
    {
        var filtered = [];
        angular.forEach(items, function(item)
        {
            filtered.push(item);
        });
        filtered.sort(function(a, b)
        {
            return (a[field] > b[field]);
        });
        if (reverse) filtered.reverse();
        return filtered;
    };
});
app.factory('service', function($http)
{
    return {
        getCardsForBoard: function()
        {
            //return the promise directly.
            return $http.get('test.json').then(function(result)
            {
                //resolve the promise as the data
                return result.data;
            });
        }
    };
});


var Widget = {

    jQuery: $,

    settings: {
        columns: '.column',
        widgetSelector: '.widget',
        handleSelector: '.widget-head',
        contentSelector: '.widget-content',
        widgetDefault: {
            movable: true,
            removable: true,
            collapsible: true,
            editable: true,
            colorClasses: ['color-yellow', 'color-red', 'color-blue', 'color-white', 'color-orange', 'color-green']
        },
        widgetIndividual: {
            intro: {
                movable: false,
                removable: false,
                collapsible: false,
                editable: false
            },
            gallery: {
                colorClasses: ['color-yellow', 'color-red', 'color-white']
            }
        }
    },

    init: function()
    {
        // this.addWidgetControls();
        this.makeSortable();
    },

    getWidgetSettings: function(id)
    {
        var $ = this.jQuery,
            settings = this.settings;
        return (id && settings.widgetIndividual[id]) ? $.extend(
        {}, settings.widgetDefault, settings.widgetIndividual[id]) : settings.widgetDefault;
    },
    makeSortable: function()
    {
        var Widget = this,
            $ = this.jQuery,
            settings = this.settings,
            $sortableItems = (function()
            {
                var notSortable = [];
                $(settings.widgetSelector, $(settings.columns)).each(function(i)
                {
                    if (!Widget.getWidgetSettings(this.id).movable)
                    {
                        if (!this.id)
                        {
                            this.id = 'widget-no-id-' + i;
                        }
                        notSortable.push('#' + this.id);
                    }
                });
                return $('> li:not(' + notSortable.join(",") + ')', settings.columns);
            })();

        $sortableItems.find(settings.handleSelector).css(
        {
            cursor: 'move'
        }).mousedown(function(e)
        {
            $sortableItems.css(
            {
                width: ''
            });
            $(this).parent().css(
            {
                width: $(this).parent().width() + 'px'
            });
        }).mouseup(function()
        {
            if (!$(this).parent().hasClass('dragging'))
            {
                $(this).parent().css(
                {
                    width: ''
                });
            }
            else
            {
                $(settings.columns).sortable('disable');
            }
        });

        $(settings.columns).sortable(
        {
            items: $sortableItems,
            connectWith: $(settings.columns),
            handle: settings.handleSelector,
            placeholder: 'widget-placeholder',
            forcePlaceholderSize: true,
            revert: 300,
            delay: 100,
            opacity: 0.8,
            containment: 'document',
            start: function(e, ui)
            {
                $(ui.helper).addClass('dragging');
            },
            stop: function(e, ui)
            {
                $(ui.item).css(
                {
                    width: ''
                }).removeClass('dragging');
                $(settings.columns).sortable('enable');
            }
        });
    }

};
